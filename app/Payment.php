<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Payment extends Eloquent
{
   protected $collection = 'payments';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
      'username', 'amount'
   ];

   /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
   protected $hidden = [
      '_id',
      'updated_at',
      'created_at',
   ];

   public function user()
   {
      return $this->belongsToMany('App\User');
   }
}
