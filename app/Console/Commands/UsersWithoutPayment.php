<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use App\User;
use App\Payment;
use Carbon\Carbon;

class UsersWithoutPayment extends Command
{
   /**
    * The name and signature of the console command.
    *
    * @var string
    */
   protected $signature = 'users:payments';

   /**
    * The console command description.
    *
    * @var string
    */
   protected $description = 'Send an email of users without payments';

   /**
    * Create a new command instance.
    *
    * @return void
    */
   public function __construct()
   {
      parent::__construct();
   }

   /**
    * Execute the console command.
    *
    * @return mixed
    */
   public function handle()
   {
      $list_users = collect();

      $payments  =  Payment::where('created_at', '>', Carbon::now()->startOfDay())->get();

      foreach ($payments as $value) {
         $list_users->push($value['username']);
      }

      $users = User::whereNotIn('username', $list_users)->count();

      Mail::to('from@example.com')->send(new SendMailable($users));
   }
}
