<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User extends Eloquent
{

   protected $collection = 'users';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
      'username', 'password', 'site'
   ];

   /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
   protected $hidden = [
      '_id',
      'updated_at',
      'created_at',
   ];

   public function payments()
   {
      return $this->belongsToMany('App\Payment');
   }
}
