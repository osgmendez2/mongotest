<?php

namespace App\Services;

use App\User;
use App\Payment;
use Carbon\Carbon;

class UserService
{

   /**
    * [agrega usuario]
    *
    * @param   array  $data  [datos necesarios para agregar un usuario]
    *
    */
   public static function addUser(array $data): void
   {
      try {
         $user = new User;
         $user->username = (int)$data['phone'];
         $user->password = $data['password'];
         $user->site = $data['site'];
         $user->save();
      } catch (\Exception $e) {
         throw ($e);
      }
   }

   /**
    * [busca usuario]
    *
    * @param   int  $username  [criterio para la busqueda]
    *
    * @return array
    */
   public static function findUser(int $username)
   {
      try {
         $user = User::where('username', $username)->first();
         return $user;
      } catch (\Exception $e) {
         throw ($e);
      }
   }

}
