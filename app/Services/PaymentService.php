<?php

namespace App\Services;

use App\Payment;

class PaymentService
{
   /**
    * [agrega pago]
    *
    * @param   array  $data  [datos necesarios para agregar un pago]
    *
    */
   public static function addPayment(array $data): void
   {
      try {
         $payment = new Payment;
         $payment->username = $data['phone'];
         $payment->amount = $data['amount'];
         $payment->save();
      } catch (\Exception $e) {
         throw ($e);
      }
   }
}
