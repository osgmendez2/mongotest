<?php

namespace App\Services;

use App\ErrorLog;
use App\Services\ErrorLogService;

class ResponseService
{

   /**
    * [parsear errores de exepciones para respuestas y loguear error]
    *
    * @param   string      $operation     [tipo de operacion realizada]
    * @param   string      $originalData  [datos originales de la peticion]
    * @param   \Exception  $e             [objeto Excepcion]
    *
    * @return  Collection         
    */
   public static function parseResponseErrors(string $originalData, \Exception $e)
   {

      try {

         $logMessage = "Problemas en operación: {$e}";
         $code = $e->getCode() ? $e->getCode() : 500;

         // se loguea en mensaje de error
         \Log::critical($logMessage);

         // se guarda el error en bbdd
         $errorMsg = array(
            'original_msg' => $originalData, 
            'error_msg' => $logMessage, 
            'state' => ErrorLog::STATES[0]
         );

         ErrorLogService::create($errorMsg);

         // se arma colleccion de respuesta
         $error = collect([
            'type' => 'error',
            'data' => json_encode(collect([
               'msg'   => $logMessage,
               'code'  => $code
            ]))
         ]);
         
         return $error;

      } catch(\Exception $e) {
         throw($e);
      }

   }

}
