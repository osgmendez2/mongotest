<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\ErrorException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Services\ResponseService;
use Exception;
use Log;

class Handler extends ExceptionHandler
{
   /**
    * A list of the exception types that are not reported.
    *
    * @var array
    */
   protected $dontReport = [
      //
   ];

   /**
    * A list of the inputs that are never flashed for validation exceptions.
    *
    * @var array
    */
   protected $dontFlash = [
      'password',
      'password_confirmation',
   ];

   /**
    * Report or log an exception.
    *
    * @param  \Exception  $e
    * @return void
    */
   public function report(Exception $e)
   {
      parent::report($e);
   }

   /**
    * Render an exception into an HTTP response.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Exception  $e
    * @return \Illuminate\Http\Response
    */
   public function render($request, Exception $e)
   {

      // if (!isset($request->data)) {
      //    return response()->json($request->data, 200);
      // }

      // exepciones predefinidas de laravel
      if ($e instanceof ModelNotFoundException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof NotFoundHttpException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof MethodNotAllowedHttpException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof QueryException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof PDOException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof InvalidArgumentException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof ErrorException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof BadMethodCallException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof FatalThrowableError) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof AuthenticationException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      if ($e instanceof OAuthServerException) {
         $error = ResponseService::parseResponseErrors($request->data, $e);
         return response()->json($error, 200);
      }

      // errores de validacion
      if ($e instanceof ValidationException) {
         Log::critical('Exception: ' . $e);
         $errors = self::getCustomMessagesByValidator($e->errors());
         return response()->json(["error" => $errors, 'codigo' => 422], 422);
      }

      $error = ResponseService::parseResponseErrors($request->data, $e);
      return response()->json($error, 200);
   }

   /**
    * [customize message errors by validator request]
    *
    * @param   array  $errors  [array of errores]
    *
    * @return  string          [errores]
    */
   static function getCustomMessagesByValidator(array $errors)
   {
      $fullErrors = '';
      foreach ($errors as $errorsOne) {
         foreach ($errorsOne as $strError) {
            $separator = ' | ';
            $fullErrors .= "{$strError} {$separator}";
         }
      }

      return $fullErrors;
   }
}
