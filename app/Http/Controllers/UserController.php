<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddUserRequest;
use App\Services\ResponseService;
use App\Services\UserService;

class UserController extends Controller
{

   public function addUser(AddUserRequest $request)
   {
      try {
         $data = $request->all();
         UserService::addUser($data);
         return response()->json('added user', 200);
      } catch (\Exception $error) {
         $error = ResponseService::parseResponseErrors(json_encode($data), $error);
         return response()->json($error, 400);
      }
   }
}
