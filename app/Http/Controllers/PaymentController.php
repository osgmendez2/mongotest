<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\PaymentService;
use App\Services\ResponseService;
use App\Http\Requests\PaymentRequest;

class PaymentController extends Controller
{

   /**
    * [agrega pagos]
    *
    * @param   PaymentRequest  $request  [datos enviados por request]
    *
    * @return  Response         [respuesta]
    */
   public function addPayment(PaymentRequest $request)
   {
      try {
         $data = $request->all();
         $user = UserService::findUser($data['phone']);
         if (!isset($user['username'])) {
            return response()->json('Error - user not found', 400);
         }
         PaymentService::addPayment($data);
         return response()->json('added payment', 200);
      } catch (\Exception $e) {
         $e = ResponseService::parseResponseErrors('error', $e);
         return response()->json($e, 400);
      }
   }
}
