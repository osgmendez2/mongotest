<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
   /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
   public function authorize()
   {
      return true;
   }

   /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
   public function rules()
   {
      return [
         'phone' => 'required|integer',
         'password' => 'required|string',
         'site' => 'required|string',
      ];
   }

   /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
   public function messages()
   {
      return [
         'phone.required'  => 'a phone is required',
         'password.required'   => 'a password is required',
         'site.required' => 'a site is required',

         'phone.integer'   => 'phone must be an integer number',
         'password.string'    => 'password must be an string',
         'site.string'  => 'password must be an string',
      ];
   }
}
