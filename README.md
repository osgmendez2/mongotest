# Instalación 

Para el desarrollo de esta api se utilizo php 7.2.11 es importante tenerlo en cuenta

```sh
composer install
```
- Verifique que las la conección a la bd este correcta en el archivo .env
- Ejecute las migraciones
```sh
php artisan migrate
```

# Test
Para consumir los servicios addUser y addPayment dirijace hasta la ruta ./consumers , abra una terminal y ejecute los comandos:

```sh
php add-user.php
php add-payment.php
```

# Via Artisan
Para ejecutar el command para la notificacion enviada por mail debe tener configuradas las credenciales de mailtrap en el archivo .env . Pueden realizar el test mediante el comando
```sh
php artisan users:payments
```
O tambien ejecutando el comando 

```sh
php artisan schedule:run
```

# DataBase

El respaldo de la base de datos se encuenta en el directorio .\db_dump

# Diagramas de Flujo

Los diagramas se encuentra ubicados en el directorio .\digramas_uml